using UnityEngine;
using System.Collections;

public class DamageControl : MonoBehaviour {
	
	GameObject gameManagerObject;
	GameManager gameManagerComponent;
	
	Color thisColour;
	

	// Use this for initialization
	void Start () {
		gameManagerObject = GameObject.FindGameObjectWithTag("GameManager");
		gameManagerComponent = gameManagerObject.GetComponent<GameManager>();
		thisColour = this.renderer.material.color;
	
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		if (gameManagerComponent.getZoneIsAlive(this.transform.root.name))
			this.renderer.material.color = thisColour;
		else
			this.renderer.material.color = Color.black;
		
	
	}
}
