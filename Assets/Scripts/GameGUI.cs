using UnityEngine;
using System.Collections;

public class GameGUI : MonoBehaviour {	
	private bool scoreMenu = false;
	GameObject gameManagerObject;
	GameManager gameManagerComponent;
	
	public Texture defaultTexture,
					damageTexture,
					seekingTexture,
					spawnTexture,
					extraBallTexture,
					speedTexture;
	private Texture currentTexture;

	
	// Use this for initialization
	void Start () {
	gameManagerObject = GameObject.FindGameObjectWithTag("GameManager");
	gameManagerComponent = gameManagerObject.GetComponent<GameManager>();
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		
		switch (gameManagerComponent.getPowerUp())
		{
		case "damage":
			currentTexture = damageTexture;
			break;
		case "seeking":
			currentTexture = seekingTexture;
			break;
		case "spawn":
			currentTexture = defaultTexture;
			break;
		case "extraball":
			currentTexture = defaultTexture;
			break;		
		case "speed":
			currentTexture = speedTexture;
			break;
		default:
			currentTexture = defaultTexture;
			break;
			
		}
	
	}
	
	void OnGUI () {
		if ((!(gameManagerComponent.GetPlay())) && (!(scoreMenu))){
			if (GUI.Button (new Rect((Screen.width / 2) - 100, (Screen.height /2)  - 125, 200, 50), "Play Game")){
				gameManagerComponent.SetPlay(true);
				gameManagerComponent.setPinballsToPlay();
				gameManagerComponent.resetScore();
				
				
			}
			if (GUI.Button (new Rect((Screen.width / 2) - 100, (Screen.height /2)  - 50, 200, 50), "High Score")){
				scoreMenu = true;
			}
			if (GUI.Button (new Rect((Screen.width / 2) - 100, (Screen.height /2)  + 25, 200, 50), "Quit Game")){
				Application.Quit();
			}
		}
		
		if (gameManagerComponent.GetPlay ()){
			GUI.Box(new Rect(15, Screen.height - 90, 200, 30), "Pinballs Remaining: " + gameManagerComponent.getPinballCount());
			GUI.Box (new Rect(15, Screen.height - 55, 200, 30), "Powerup: ");
			GUI.DrawTexture(new Rect(150, Screen.height - 65, 50, 50), currentTexture);
			GUI.Box(new Rect(Screen.width - 215, Screen.height - 90, 200, 30), "Score: " + gameManagerComponent.getScore());
			GUI.Box (new Rect(Screen.width - 215, Screen.height - 55, 200, 30), "High Score: " + gameManagerComponent.getNextScore());
		}
		
		if (scoreMenu){
			int[] printScores = new int[10];
			printScores = gameManagerComponent.getHighScore();
			GUI.Box (new Rect((Screen.width /2) - 100, (Screen.height /2) - 175, 200, 50), "High Scores");
			for (int i = 9; i>=0; i--){
				GUI.Box (new Rect((Screen.width / 2) - 100, (Screen.height / 2) - (100 - 30 * i), 200, 20), printScores[i].ToString ());
			}
			if (GUI.Button (new Rect((Screen.width / 2) - 100, (Screen.height /2)  + 200, 200, 50), "Return")){
				scoreMenu = false;
			}
		}
	}
}
