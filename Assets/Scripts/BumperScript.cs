using UnityEngine;
using System.Collections;

public class BumperScript : MonoBehaviour {
	
	GameObject gameManagerObject;
	GameManager gameManagerComponent;
	
	public float bounciness;
	public int thisPoints;
	
	double haloTime;
	
	AudioSource bumperAudio;
	
	// Use this for initialization
	void Start () {
		
		gameManagerObject = GameObject.FindGameObjectWithTag("GameManager");
		gameManagerComponent = gameManagerObject.GetComponent<GameManager>();
		haloTime = Time.time;
		
		bumperAudio = GetComponent(typeof(AudioSource)) as AudioSource;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	void FixedUpdate () {
		if (Time.time < haloTime)
			this.light.enabled = true;
		else
			this.light.enabled = false;		
	}
	
	
	void OnCollisionEnter (Collision collidedObject) {		
		if (collidedObject.transform.name == "Pinball(Clone)" && gameManagerComponent.getZoneIsAlive(transform.root.name)){
			collidedObject.rigidbody.AddForce(collidedObject.contacts[0].normal * bounciness, ForceMode.VelocityChange);
			gameManagerComponent.setScore(thisPoints);
			haloTime = Time.time + 0.3;	
			bumperAudio.Play();
		}
	}
}
