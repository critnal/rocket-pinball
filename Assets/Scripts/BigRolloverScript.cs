using UnityEngine;
using System.Collections;

public class BigRolloverScript : MonoBehaviour {
	
	public int thisPoints;
	
	GameObject gameManagerObject;
	GameManager gameManagerComponent;
	
	AudioSource rolloverAudio;

	// Use this for initialization
	void Start () {
		
		gameManagerObject = GameObject.FindGameObjectWithTag("GameManager");
		gameManagerComponent = gameManagerObject.GetComponent<GameManager>();
		
		this.light.enabled = false;
		
		rolloverAudio = GetComponent(typeof(AudioSource)) as AudioSource;
	
	}
	
	// Update is called once per frame
	void FixedUpdate () {
	}
	
	
	void OnTriggerEnter (Collider collidedObject) {
		
		if (collidedObject.transform.name == "Pinball(Clone)" 
			&& gameManagerComponent.getZoneIsAlive(transform.root.name)
			&& this.light.enabled == false)
		{
			gameManagerComponent.setScore(thisPoints);
			gameManagerComponent.bigRolloverManager(this.transform.name);
			if (gameManagerComponent.bigRolloverManager(this.transform.name))
				this.light.enabled = false;
			else
				this.light.enabled = true;
			rolloverAudio.Play();
			
		}
	}
}
