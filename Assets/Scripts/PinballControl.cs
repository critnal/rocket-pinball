using UnityEngine;
using System.Collections;

public class PinballControl : MonoBehaviour {
	
	public float maxVelocity;
	private float sqrMaxVelocity;
	
	public float explosionPower;
	
	
	// Use this for initialization
	void Start () {
		
	}
	
	
	void Awake () {
		sqrMaxVelocity = maxVelocity * maxVelocity;
	}
	
	
	void FixedUpdate () {
		if (this.rigidbody.velocity.sqrMagnitude > sqrMaxVelocity)
			this.rigidbody.velocity = this.rigidbody.velocity.normalized * maxVelocity;
	}
}
